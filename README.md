# gin-work

#### 介绍
本项目为gin框架开发的脚手架，旨在可以快速开发应用服务。


#### 软件架构
软件架构说明 

```
├─.gitignore
├─go.mod
├─main.go
├─README.md
├─web
├─utils
├─routers
├─model
├─config
├─app
├─api
├─middleware
```

* app -- 基础逻辑
* config -- 配置文件参数
* api -- 前后端分离接口
* model -- 数据库实体类
* middleware -- 中间件
* routers -- 路由
* utils -- 工具类
* web -- 前端页面



#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


 
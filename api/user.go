package api

import (
	"gin-work/app/result"

	"github.com/gin-gonic/gin"
)

func UserInfo(c *gin.Context) {
	result.OKWithMsg(c, "查询user")
}

type User struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func UserList(c *gin.Context) {
	data := []*User{
		&User{Name: "xiao", Age: 20},
		&User{Name: "fan", Age: 34},
	}

	result.OKWithList(c, 10, 1, 20, data)
}

package result

import (
	"fmt"
	"reflect"
)

// 分页数据
type PagingData struct {
	List       interface{} `json:"list"`
	Pagination struct {
		Total        int64       `json:"total"`        // 总页数
		CurrentPage  int64       `json:"currentPage"`  // 当前页码
		PrePageCount int64       `json:"prePageCount"` // 每一页数量
		Ext          interface{} `json:"ext"`          // 额外字段
	} `json:"pagination"`
}

// 返回分页列表
func ResponseListResultExt(total int64, currpage int64, pagesize int64, list interface{}, ext map[string]interface{}) *PagingData {
	if nil == list {
		list = []interface{}{}
	} else {
		rt := reflect.TypeOf(list)
		switch rt.Kind() {
		case reflect.Slice:
		case reflect.Array:
		default:
			fmt.Println("ResponseListResult list 需要格式为slice or array，当前类型:", rt.Kind())
		}
	}

	page := &PagingData{}
	page.List = list
	page.Pagination.Total = total
	page.Pagination.CurrentPage = currpage
	page.Pagination.PrePageCount = pagesize
	page.Pagination.Ext = ext

	return page
}

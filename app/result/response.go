package result

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Response struct {
	Code    int         `json:"code"`    // 业务编码
	Message string      `json:"message"` // 错误描述
	Data    interface{} `json:"data"`    // 成功时返回的数据
}

func Result(c *gin.Context, code int, msg string, data interface{}) {
	c.JSON(http.StatusOK, Response{
		code,
		msg,
		data,
	})
}

func Error(code int, msg string) Response {
	return Response{
		code,
		msg,
		nil,
	}
}

// 【成功】
func OK(c *gin.Context) {
	Result(c, Success.Code, Success.Message, nil)
}

// 【成功】带有数据
func OKWithData(c *gin.Context, data interface{}) {
	Result(c, Success.Code, Success.Message, data)
}

// 【成功】带有提示
func OKWithMsg(c *gin.Context, msg string) {
	Result(c, Success.Code, msg, nil)
}

// 【成功】带有列表数据
func OKWithList(c *gin.Context, total int64, currpage int64, pagesize int64, list interface{}) {
	r := ResponseListResultExt(total, currpage, pagesize, list, nil)
	c.JSON(http.StatusOK, r)
	c.Abort()
}

// 【失败】
func Fail(c *gin.Context, err Response) {
	Result(c, err.Code, err.Message, nil)
}

// 【失败】带有自定义错误信息
func FailWithMsg(c *gin.Context, err Response, msg string) {
	Result(c, err.Code, msg, nil)
}

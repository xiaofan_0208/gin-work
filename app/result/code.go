package result

var (
	Success      = Error(0, "Success")
	UnknownError = Error(-1, "错误")
	// 系统错误
	ErrForbidden        = Error(10000, "非法操作")
	InternalServerError = Error(10001, "内部服务器错误")
	ErrBind             = Error(10002, "请求参数错误")
	ErrTokenSign        = Error(10003, "签名错误")
	ErrEncrypt          = Error(10004, "加密错误")
	// 数据库错误
	ErrDatabase = Error(20100, "数据库错误")
	ErrFill     = Error(20101, "从数据库填充 struct 时发生错误")
	// 认证错误
	ErrValidation    = Error(20201, "验证失败")
	ErrTokenInvalid  = Error(20202, "jwt 是无效的")
	ErrTokenNotFound = Error(20203, "token 不存在")
	// 用户错误
	ErrParamIsMiss       = Error(20301, "参数不能为空")
	ErrUserNotFound      = Error(20302, "用户没找到")
	ErrPasswordIncorrect = Error(20303, "密码错误")
	ErrUserPhone         = Error(20304, "用户手机号不合法")
	ErrUserCaptcha       = Error(20305, "用户验证码有误")
)

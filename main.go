package main

import (
	"gin-work/routers"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	routers.Routers(r)

	r.Run()
}

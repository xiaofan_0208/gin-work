package routers

import (
	"gin-work/api"

	"github.com/gin-gonic/gin"
)

func Routers(router *gin.Engine) {

	router.GET("/userinfo", api.UserInfo)
	router.GET("/userlist", api.UserList)
}
